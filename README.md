# KDE Service Menus

A collection of useful KDE Plasma5 service menu entries

# Installation
Copy one or more of these *.desktop files to ~/.local/share/kservices5/ and restart dolphin.

# Supported Actions

## Java
Service menu to run *.jar files with java

## NVidia Optimus
Service menue to offload applications to the dGPU.
### Suported configurations
- NVidia closed source driver: 450 and above
- Bumblebee Optirun as well as Primusrun

## Open Source hybrid GPU Support
- Only OpenGL by using DRI_PRIME=1

## MangoHUD
- Support for mangohud and mangohud --dlsym

## Zink
- Run OpenGL based applications via Zink for nvidia systems with the closed source driver. (Requires at least Mesa 21.3)

## Feral Gamemode
- Support for Feral Gamemode
- Support for MangoHUD plus Feral Gamemode. In case you want to use both at the same time

## TVNamer
- install tvnamer to use this
